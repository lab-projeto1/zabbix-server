#!/usr/bin/env bash

TAG="1.0.0"
data="{\"name\": \"Release $TAG\", \"tag_name\": \"$TAG\", \"description\": \"New release for tag $TAG\"}"
echo $data