#!/usr/bin/env bash

data="{\"name\": \"Release $TAG\", \"tag_name\": $TAG, \"description\": \"New release for tag $TAG\"}" 
echo $data
curl --request POST "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/releases" --data "$data" \
--header "PRIVATE_TOKEN: $CI_JOB_TOKEN"