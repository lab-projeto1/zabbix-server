#!/usr/bin/env bash

get_tags=""
GET_VERSION=$(grep -oP "ZBX_VERSION=.*" Dockerfile | cut -d = -f 2)
BASE_VERSION=$(echo $GET_VERSION | tr -d '\r')
GET_TAGS=$(curl -s "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/repository/tags?order_by=version&search=*$BASE_VERSION*" \
--header "PRIVATE_TOKEN: $$CI_JOB_TOKEN")
last_version=$(echo $GET_TAGS | jq -r '.[0].name')
if [ -z "$last_version" ] || [ "$last_version" == "null" ]
then
    new_version=$BASE_VERSION.0
    echo $new_version
else
    major_version=$(echo $last_version | cut -d . -f 1)
    minor_version=$(echo $last_version | cut -d . -f 2)
    patch_version=$(echo $last_version | cut -d . -f 3)
    my_patch_version=$(echo $last_version | cut -d . -f 4)
    ((my_patch_version++))
    new_version=$major_version.$minor_version.$patch_version.$my_patch_version
    echo $new_version
fi